# README #

Welcome to Employee server app. This application provides some functionalities
for managing some tables. User have to chose an action from web page. After the user has performed the action , the result will be displayed as plain text on page.

### About###

* Author Goineanu Serghei
* Version 0.0.1
* https://SergheiGoineanu@bitbucket.org/SergheiGoineanu/vending-machine.git

### Features ###

* Not yet

### Tests ###

* In development

### Changelog ###
* Initial structure


### This readme was written for ###
* Cosofret Constantin
* Florin Ciubotariu
* Others
