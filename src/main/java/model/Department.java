package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Department {
	@Id
	private String name;
	private int nr_employee;
	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column
	public int getNr_employee() {
		return nr_employee;
	}

	public void setNr_employee(int nr_employee) {
		this.nr_employee = nr_employee;
	}
}
