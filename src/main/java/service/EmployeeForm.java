package service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Department;
import model.Employee;
import model.Project;
import model.Social_id;
import dao.DepartmentDao;
import dao.EmployeeDao;
import dao.ProjectDao;
import dao.Social_id_Dao;


public class EmployeeForm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		EmployeeDao employeeDao = new EmployeeDao();
		Employee employee = new Employee();
		employeeDao.listAll();
		resp.getOutputStream();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Department department = new Department();
		DepartmentDao deptDAO = new DepartmentDao();
		Project project = new Project();
		ProjectDao projDAO = new ProjectDao();
		Social_id socialid = new Social_id();
		Social_id_Dao socDAO = new Social_id_Dao();
		Employee employee = new Employee();
		EmployeeDao empDAO = new EmployeeDao();
		employee.setName("Jhon Smith");
		department.setName("HR");
		project.setProject_id("P1");
		socialid.setId(7777777);
		socialid.setType("CI");
		req.setAttribute("Employee", empDAO.newEmployee(employee));
		req.setAttribute("Department", deptDAO.newDepartment(department));
		req.setAttribute("Project name", projDAO.newProject(project));
		req.setAttribute("Social ID", socDAO.newSocial_id(socialid));
	}

	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Employee employee = new Employee();
		EmployeeDao empDAO = new EmployeeDao();
		empDAO.deleteEmployee(employee);
		resp.sendRedirect(req.getContextPath());
	}

}