package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Social_id;
import model.Social_id;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Social_id_Dao {
	private static SessionFactory factory;
	private static List<Social_id> social_ids=new ArrayList<Social_id>();

	public Social_id newSocial_id(Social_id social_id){
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.save(social_id);
			social_ids.add(social_id);
			transaction.commit();
			session.close();
		return social_id;
	}
	public Social_id deleteSocial_id(Social_id social_id) {
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(social_id);
			social_ids.remove(social_id);
			transaction.commit();
			session.close();
		return social_id;
	}

	public Iterator<Social_id> listAll() {
		Iterator<Social_id> socialIterator = social_ids.iterator();
		return socialIterator;
	}
}
