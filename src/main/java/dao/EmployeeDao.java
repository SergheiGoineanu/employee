package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Employee;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EmployeeDao {
	private static SessionFactory factory;
	private static List<Employee> projects=new ArrayList<Employee>();

	public Employee newEmployee(Employee employee){
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.save(employee);
			projects.add(employee);
			transaction.commit();
			session.close();
		return employee;
	}
	public Employee deleteEmployee(Employee employee) {
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(employee);
			projects.remove(employee);
			transaction.commit();
			session.close();
		return employee;
	}

	public Iterator<Employee> listAll() {
		Iterator<Employee> emplpyeeIterator = projects.iterator();
		return emplpyeeIterator;
	}
}
