package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Project;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ProjectDao {
	private static SessionFactory factory;
	private static List<Project> projects=new ArrayList<Project>();

	public Project newProject(Project project){
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.save(project);
			projects.add(project);
			transaction.commit();
			session.close();
		return project;
	}
	public Project deleteProject(Project project) {
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(project);
			projects.remove(project);
			transaction.commit();
			session.close();
		return project;
	}

	public Iterator<Project> listAll() {
		Iterator<Project> projectIterator = projects.iterator();
		return projectIterator;
	}
}
