package dao;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Department;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DepartmentDao {
	private static SessionFactory factory;
	private static List<Department> departments=new ArrayList<Department>();
	
	public Department newDepartment(Department department){
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.save(department);
			departments.add(department);
			transaction.commit();
			session.close();
		return department;
	}
	public Department deleteDepartment(Department dapartment) {
		Session session = factory.openSession();
		Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(dapartment);
			departments.remove(dapartment);
			transaction.commit();
			session.close();
		return dapartment;
	}

	public Iterator<Department> listAll() {
		Iterator<Department> departmentIterator = departments.iterator();
		return departmentIterator;
	}
}
